const sqlite = require('sqlite3');
const path = require('path');

// Prints calls to DB. For debug purposes.
const SHOW_DB_CALLS = false;

///////////////////////////
///// CALL WHEN READY /////
///////////////////////////

// functions to call when database has been connected to
let callbacks = [ ];
// if the database has been connected to
let ready = false;

/**
 * Adds function to called when database has been connected to, or immediately if it is already connected.
 * @param {()=>} callback
 */ 
function callWhenReady(callback) {
    if (ready) {
        callback();
    } else {
        callbacks.push(callback);
    }
}

/**
 * Called when database has been connected to.
 */
function readyToGo() {
    if (ready) {
        return;
    }

    ready = true;

    for (let cb of callbacks) {
        cb();
    }

    callbacks = [ ];
}

////////////////////
///// DATABASE /////
////////////////////

// connect to DB
let db = new sqlite.Database(path.join(__dirname, '../data.db'), (err)=>{
    if (err) {
        console.error(`Error connecting to database ${Settings.settings.dbPath}: ${err}`);
        process.exit(1);
    }

    readyToGo();
});

/////////////////////////
///// DB OPERATIONS /////
/////////////////////////

/**
 * Executes a GET select statement.
 * @param {string} stmt 
 * @param {string|number|(string|number)[]} params
 * @returns {Promise<any>}
 */
function queryDBGet(stmt, params) {
    return new Promise((resolve, reject)=>{
        let callback = function(err, row) {
            if (err) {
                console.error(`Error doing DB get: ${err}`);
                console.error(stmt);
                reject(err);
            } else {
                resolve(row);
            }
        }

        if (SHOW_DB_CALLS) {
            console.log(`Statement: ${stmt}`);
            console.log(`Params: ${JSON.stringify(params)}`);
        }

        if (params) {
            db.get(stmt, params, callback);
        } else {
            db.get(stmt, callback);
        }
    });
}

/**
 * Executes an ALL select statement.
 * @param {string} stmt 
 * @param {string|number|(string|number)[]} params 
 * @returns {Promise<any[]>}
 */
function queryDBAll(stmt, params) {
    return new Promise((resolve, reject)=>{
        let callback = function(err, rows) {
            if (err) {
                console.error(`Error doing DB all: ${err}`);
                console.error(stmt);
                reject(err);
            } else {
                resolve(rows);
            }
        }

        if (SHOW_DB_CALLS) {
            console.log(`Statement: ${stmt}`);
            console.log(`Params: ${JSON.stringify(params)}`);
        }

        if (params) {
            db.all(stmt, params, callback);
        } else {
            db.all(stmt, callback);
        }
    });
}

/**
 * Executes a RUN statement.
 * @param {string} stmt 
 * @param {string|number|(string|number)[]} params
 * @returns {Promise<{changes:number,lastID:number}>}
 */
function queryDBRun(stmt, params) {
    return new Promise((resolve, reject)=>{
        let callback = function(err) {
            if (err) {
                console.error(`Error doing DB run: ${err}`);
                console.error(stmt);
                reject(err);
            } else {
                resolve({
                    lastID:this.lastID,
                    changes:this.changes                    
                });
            }
        }

        if (SHOW_DB_CALLS) {
            console.log(`Statement: ${stmt}`);
            console.log(`Params: ${JSON.stringify(params)}`);
        }

        if (params) {
            db.run(stmt, params, callback);
        } else {
            db.run(stmt, callback);
        }
    });
}

/**
 * Closes connection to the database.
 * @returns {Promise<void>}
 */
function close() {
    return new Promise((resolve, reject)=>{
        db.close((err)=>{
            if (err) {
                console.error(`Error closing database: ${err}`);
                reject();
            } else {
                resolve();
            }
        });
    });
}

module.exports = {
    queryDBGet,
    queryDBAll,
    queryDBRun,
    callWhenReady,
    close
};