const DataFunctions = require('./data-functions');
const DiscordHandler = require('./discord-handler');
const Config = require('./config');
const RapidAPI = require('./rapid-api');

const config = Config.getConfig();
const api = new RapidAPI(config.rapidAPIKey);

// if app is in debug mode; used for output
let isDebug = process.argv.includes('-debug');

/**
 * Prints message if in debug mode.
 * @param { string } txt Message to print
 */
function log(txt) {
    if (isDebug) {
        console.log(txt);
    }
}

/**
 * Parses Rapid API data and turns it into MessageEmbed data.
 * @param { [WorldTotalData,ByCountryData] } data 
 */
function dataToMessageData(data) {
    let fields = [ ];

    fields.push({
        title:'World - Total Cases',
        value:`${data[1].total_cases} (+${data[1].new_cases})`
    });

    fields.push({
        title:'World - Total Deaths',
        value:`${data[1].total_deaths} (+${data[1].new_deaths})`
    });

    // remove bad data and ensure list is sorted
    data[0].countries_stat = data[0].countries_stat.filter(country=>country.country_name !== '').sort((a, b)=>(+(b.cases.replace(/,/g,'')))-(+(a.cases.replace(/,/g,''))));

    for (let i = 0; i < config.countries; i++) {
        fields.push({
            title:`${data[0].countries_stat[i].country_name} - Total Cases`,
            value:`${data[0].countries_stat[i].cases} (+${data[0].countries_stat[i].new_cases})`
        });

        fields.push({
            title:`${data[0].countries_stat[i].country_name} - Total Deaths`,
            value:`${data[0].countries_stat[i].deaths} (+${data[0].countries_stat[i].new_deaths})`
        });
    }

    let messageConfig = config.messageConfig || { };

    /** @type { EmbedData } */
    let msgData = {
        title:messageConfig.title,
        color:messageConfig.color,
        thumbnail:messageConfig.thumbnail,
        image:messageConfig.image,
        footer:messageConfig.footer,
        inline:messageConfig.inline,
        fields
    }

    return msgData;
}

/**
 * Sends update to specified channels.
 * @param {string|string[]} channels 
 */
async function sendToChannels(channels) {
    // get data
    log('Getting data');

    try {
        var data = await Promise.all([
            api.getCasesByCountry(),
            api.getWorldTotal()
        ]);
    } catch (err) {
        console.error(`Error getting data: ${err}`);
    }

    // send data    
    if (data) {
        log('Preparing data');
        const msgData = dataToMessageData(data);

        log('Sending message');            
        DiscordHandler.sendEmbedMessage(msgData, channels);
    } else {
        log('No data');
    }

    log('Done');
}

/**
 * Finds servers waiting for updates and sends to them.
 */
async function sendToServers() {
    log('Getting servers to send to');
    let channels = await DataFunctions.getChannelsForUpdates();

    if (channels.length > 0) {
        console.log(`Sending to ${channels.length} servers`);
        sendToChannels(channels);
    }
}

module.exports = {
    sendToChannels,
    sendToServers
}