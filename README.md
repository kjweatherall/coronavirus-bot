# CORONAVIRUS BOT
Coronavirus Bot is a bot for the Discord platform that regularly retrieves the latest coronavirus data and sends it to registered servers. The data is retrieved from https://rapidapi.com/astsiatsko/api/coronavirus-monitor.

Coronavirus Bot is build using Node.js with the Discord.js for interacting with Discord and SQLite for storing data locally.

## Installation
- Run "npm install" to prepare
- Create and fill in required data in the config.json file (more details below)
- Use "npm start" to start the bot

## config.json
The data that must be included in the config file is:
- rapidAPIKey: Your API key for RapidAPI
- discordKey: Your Discord Bot key
- countries: The number of countries to display data for (max 11)

Optional data:
- messageConfig: Data specifying how the message with the data should be sent to Discord. This includes the optional parameters:
    - title: Title of message
    - color: Colour of the strip to the left side of the message
    - thumbnail: URL to image to include within message
    - footer: Text to include at the bottom of the message
    - inline: Boolean of if data should be displayed in columns
- superusers: A list of Discord user IDs as strings for users who are allowed to control the bot

## Using Bot
- Add bot to your server as you would with any Discord bot
- Register a channel to receive updates by tagging the bot and using "add [x]". This will have the bot send updates to the channel every x hours. If no x is specified, a default of 24 hours is used.
- To stop the bot from sending messages to the channel, tag the bot and use "remove"
- To change the frequency of the updates, tag the bot and use "frequency <x>", where x is the frequency (in hours) that messages are sent
- You can get the latest data by tagging the bot and using "now"
N.B. Only Discord users with admin permissions can control the bot.