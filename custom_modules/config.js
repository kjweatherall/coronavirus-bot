const fs = require('fs');

/** @type { Config } */
var config = null;

/**
 * Validates loaded config.
 */
function validateConfig() {
    if (!config.rapidAPIKey) {
        throw 'No rapidAPIKey value in config';
    }
    if (!config.discordKey) {
        throw 'No discordKey value in config';
    }

    // add defaults
    if (!config.countries) {
        config.countries = 10;
    }
    if (!config.messageConfig) {
        config.messageConfig = { };
    }
    if (!config.superusers) {
        config.superusers = [ ];
    }
}

/**
 * Loads config file.
 * @param {string} filePath 
 */
function load(filePath) {
    if (!fs.existsSync(filePath)) {
        throw `File ${filePath} does not exist`;
    }

    config = require(filePath);

    validateConfig();

    return config;
}

function getConfig() {
    return config;
}

module.exports = {
    load, getConfig
};