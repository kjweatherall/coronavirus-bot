const DataHandler = require('./data-handler');

// if module is ready to use
let ready = false;
// functions to call when module is ready
let callbacks = [ ];

const Functions = {
    /**
     * Gets a list of channel IDs for each channel that needs updates.
     * @returns {Promise<string[]>}
     */
    async getChannelsForUpdates() {
        let now = new Date().getTime() / 60000;

        let row = await DataHandler.queryDBGet('SELECT * FROM channels');
        console.log(now - row.last_sent);

        let results = await DataHandler.queryDBAll(`SELECT channelID FROM channels WHERE last_sent+frequency<${now}`)
        return results.map(c=>c.channelID);
    },
    /**
     * Sets the "last sent" time for a channel.
     * @param {string} channelID
     */
    async updateLastSent(channelID) {
        let now = new Date().getTime() / 60000;
        let results = DataHandler.queryDBRun(`UPDATE channels SET last_sent=${now} WHERE channelID=?`, channelID)
        return results.changes > 0;
    },
    /**
     * Adds a channel with the specified frequency.
     * @param {string} channelID 
     * @param {number} frequency
     */
    async addChannel(channelID, frequency = 24) {
        if (typeof frequency !== 'number') {
            throw 'Frequency for channel is not a number.';
        } else {
            frequency = (frequency >>> 0);
        }

        if (frequency <= 0) {
            throw 'Frequency for channel was 0 or less.';
        }

        let results = await DataHandler.queryDBRun(`INSERT OR IGNORE INTO channels(channelID,frequency) VALUES(?,?)`, [ channelID, frequency * 60 ]);
        return results.changes > 0;
    },
    /**
     * Removes a channel.
     * @param {string} channelID 
     */
    async removeChannel(channelID) {
        let results = await DataHandler.queryDBRun('DELETE FROM channels WHERE channelID=?', channelID);
        return results.changes > 0;
    },
    /**
     * Sets the frequency for a channel.
     * @param {string} channelID 
     * @param {number} frequency 
     */
    async setFrequency(channelID, frequency) {
        if (typeof frequency !== 'number') {
            throw 'Freqency to set for channel is not a number.';
        } else {
            frequency = (frequency >>> 0);
        }

        if (frequency <= 0) {
            throw 'Frequency for channel is 0 or less.';
        }

        let results = await DataHandler.queryDBRun(`UPDATE channels SET frequency=${frequency * 60} WHERE channelID=?`, channelID);
        return results.changes > 0;
    },
    /**
     * Adds a callback to be called when module is ready, or immediately if the module is ready.
     * @param {()=>void} cb 
     */
    callWhenReady(cb) {
        if (ready) {
            cb();
        } else {
            callbacks.push(cb);
        }
    }
};

/**
 * Called when module is ready.
 */
function onReady() {
    if (ready) {
        return;
    }

    ready = true;
    callbacks.forEach(cb=>cb());
    callbacks = [ ];
}

/**
 * Sets up database once it has been connected to.
 */
DataHandler.callWhenReady(()=>{
    DataHandler.queryDBRun('CREATE TABLE IF NOT EXISTS channels(channelID TEXT UNIQUE, frequency INTEGER DEFAULT 1440, last_sent INTEGER DEFAULT 0)')
    .then(onReady)
    .catch(err=>{
        console.error(`Error creating channels table: ${err}`);
        process.exit(1);
    });
});

module.exports = Functions;