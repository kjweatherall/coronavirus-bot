const Discord = require('discord.js');
const DataFunctions = require('./data-functions');

// Discord client
const client = new Discord.Client();

/**
 * Sends a mesasge to a channel or channels.
 * @param {string | Discord.EmbedMessage} msg 
 * @param {string|string[]} channels
 * @returns { Discord.Message[] }
 */
async function sendMessage(msg, channels) {
    if (!client) {
        throw 'Discord client not connected';
    }

    if (!Array.isArray(channels)) {
        channels = [ channels ];
    }

    let messages = [ ];

    for (let i = 0; i < channels.length; i++) {
        /** @type { Discord.TextChannel } */
        let channel = client.channels.cache.get(channels[i]);
        if (!channel) {
            console.error(`Cannot find channel ${channels[i]}`);
            messages.push(null);
        } else {
            try {
                var message = await channel.send(msg);
                DataFunctions.updateLastSent(channels[i])
            } catch (err) {
                console.error(`Error sending message: ${err}`);
                var message = null;
            }

            messages.push(message);
        }
    }
    
    return messages;
}

/**
 * Creates an EmbedMessage from data.
 * @param {EmbedData} data
 * @returns { Discord.MessageEmbed }
 */
function createEmbedMessage(data) {
    // no data
    if (!data) {
        throw 'No data for embed';
    }

    // check required data
    if (!data.fields || data.fields.length === 0) {
        throw 'No data fields specified';
    }

    if (!Array.isArray(data.fields)) {
        throw 'Data fields is not an array';
    }

    // embed to return
    let embed = new Discord.MessageEmbed();

    // apply message settings from config file
    if (data.color) {
        embed.setColor(data.color);
    }
    if (data.title) {
        embed.setTitle(data.title);
    }
    if (data.thumbnail) {
        embed.setThumbnail(data.thumbnail);
    }
    if (data.image) {
        embed.setImage(data.image);
    }
    if (data.footer) {
        embed.setFooter(data.footer);
    }

    // add stocks to message
    data.fields.forEach(field=>{
        // add to message
        embed.addField(field.title, field.value, data.inline || false);
    });

    return embed;
}

/**
 * Sends a MessageEmbed for the specified data to the specified channels.
 * @param {Message} data
 * @param {string|string[]} channels
 */
async function sendEmbedMessage(data, channels) {
    if (!data) {
        throw 'No data to make embed message with';
    }

    let msg = createEmbedMessage(data);
    return sendMessage(msg, channels);
}

/**
 * Disconnects client from Discord.
 */
function disconnect() {
    client.destroy();
}

/**
 * Connects client to Discord.
 * @param {string} apiKey 
 */
function connect(apiKey, msgCallback) {
    return new Promise((resolve, reject)=>{
        client.on('ready', ()=>{
            resolve();
        });

        if (msgCallback) {
            client.on('message', msg=>msgCallback(msg, client.user.id));
        }

        client.on('error', err=>{
            reject(err);
        });

        client.login(apiKey);
    });
}

module.exports = {
    connect,
    sendMessage,
    sendEmbedMessage,
    disconnect
};