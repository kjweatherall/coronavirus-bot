// structure of the config file
interface Config {
    // api key for Rapid API
    rapidAPIKey: string;
    // api key for Discord
    discordKey: string;
    // config for messages
    messageConfig?: {
        title?: string;
        color?: string;
        thumbnail?: string;
        image?: string;
        footer?: string;
        inline?: boolean;
    },
    // number of countries to send data of
    countries: number;
    superusers?: string[];
}

// data for each country in By Country data
interface CountryStat {
    country_name: string;
    cases: string;
    deaths: string;
    region: string;
    total_recovered: string;
    new_deaths: string;
    new_cases: string;
    serious_critical: string;
}

// API data for By Country request
interface ByCountryData {
    countries_stat:CountryStat[];
    statistic_taken_at:string;
}

// API data for World Total request
interface WorldTotalData {
    total_cases: string;
    total_deaths: string;
    total_recovered: string;
    new_cases: string;
    new_deaths: string;
    statistic_taken_at: string;
}

// data for MessageEmbed
interface Message {
    fields: Field[];
    title?: string;
    color?: string;
    thumbnail?: string;
    image?: string;
    footer?: string;
    inline?: boolean;
}