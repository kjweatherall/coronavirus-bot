// app config
const path = require('path');
const config = require('./custom_modules/config').load(path.join(__dirname, 'config.json'));

// modules
const DataFunctions = require('./custom_modules/data-functions');
const DiscordActions = require('./custom_modules/discord-actions');
const DiscordHandler = require('./custom_modules/discord-handler');

// include for autocomplete
const Discord = require('discord.js');

const COMMANDS = [ 'add', 'remove', 'frequency', 'now' ];

/**
 * Called when a message is received from Discord.
 * @param {Discord.Message} msg
 */
async function processMessage(msg, clientID) {
    if (msg.mentions.members.find(user=>user.id === clientID)) {
        let content = msg.content.split(' ').filter(c=>!c.startsWith('<@!'));
        let cmd = content[0] ? content[0].toLowerCase() : '';

        if (!msg.member.hasPermission('ADMINISTRATOR')) {
            return;
        }

        switch (cmd) {
            case 'add':{ // adds a channel
                if (content.length > 2) {
                    return msg.reply('Wrong number of parameters. Usage: add [frequency]');
                } else if (content.length === 2) {
                    var frequency = +content[1];
                    if (isNaN(frequency)) {
                        return msg.reply(`Expected a number for frequency. Got "${content[1]}".`);
                    } else if (frequency < 1) {
                        return msg.reply(`Expected a frequency of at least 1.`);
                    }
                } else {
                    var frequency = 24;
                }

                let results = await DataFunctions.addChannel(msg.channel.id, frequency);
                if (results) {
                    await msg.reply(`Added channel with frequency of ${frequency} hours`);
                    DiscordActions.sendToChannels(msg.channel.id);
                } else {
                    msg.reply(`Channel was already added`);
                }
                break;
            }
            case 'remove':{ // removes a channel
                if (content.length !== 1) {
                    msg.reply('Wrong number of arguments. Usage: remove');
                } else {
                    let results = await DataFunctions.removeChannel(msg.channel.id);
                    if (results) {
                        msg.reply('Channel removed');
                    } else {
                        msg.reply("Channel hasn't been added");
                    }
                }
                break;
            }
            case 'frequency':{ // sets frequency for channel
                if (content.length !== 2) {
                    msg.reply('Wrong number of arguments. Usage: frequency <frequency>');
                } else {
                    let frequency = +content[1];
                    if (isNaN(frequency)) {
                        return msg.reply(`Expected a number for frequency. Got "${content[1]}".`);
                    } else if (frequency < 1) {
                        return msg.reply(`Expected a frequency of at least 1.`);
                    }

                    try {
                        let results = await DataFunctions.setFrequency(msg.channel.id, frequency);
                        if (results) {
                            msg.reply(`Frequency set to ${frequency} hours`);
                        } else {
                            msg.reply("Channel has't been added");
                        }
                    } catch (err) {
                        console.error(`Error setting frequency: ${err}`);
                        msg.reply('Error setting frequency');
                    }
                }
                break;
            }
            case 'now':{
                if (content.length !== 1) {
                    msg.reply('Wrong number of arguments. Usage: now');
                } else {
                    DiscordActions.sendToChannels(msg.channel.id);
                }
                break;
            }
        }
    }
}

// start app when connected to database
DataFunctions.callWhenReady(async ()=>{
    // connect to Discord
    try {
        await DiscordHandler.connect(config.discordKey, processMessage);
    } catch (err) {
        console.error(`Error connecting to Discord: ${err}`);
        process.exit(1);
    }

    console.log('Ready');

    // start running
    DiscordActions.sendToServers();
    setInterval(DiscordActions.sendToServers, 60000);
});