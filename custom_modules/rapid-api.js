const https = require('https');

// API endpoints
const CASES_BY_COUNTRY = "/coronavirus/cases_by_country.php";
const WORLD_TOTAL = "/coronavirus/worldstat.php";

/**
 * Requests data from API.
 * @param { https.RequestOptions } opts
 * @returns { Promise<ByCountryData|WorldTotalData> }
 */
function getData(opts) {
    return new Promise((resolve, reject)=>{
        let req = https.request(opts, res=>{
            if (res.statusCode !== 200) {
                return reject(`Rapid API status code was ${res.statusCode}`);
            }

            let chunks = [ ];
            res.on('data', chunk=>chunks.push(chunk));
            res.on('end', ()=>{
                let data = Buffer.concat(chunks);
                resolve(JSON.parse(data.toString()));
            });
            res.on('error', err=>reject(err));
        });

        req.on('error', err=>reject(err));

        req.end();
    });
}

/**
 * Creates RequestOptions object for API request.
 * @param { string } key Rapid API key
 * @param { string } path URL path for API
 * @returns { https.RequestOptions }
 */
function makeOpts(key, path) {
    const opts = {
        method:'GET',
        hostname:'coronavirus-monitor.p.rapidapi.com',
        port:null,
        path,
        headers:{
            "x-rapidapi-host": "coronavirus-monitor.p.rapidapi.com",
		    "x-rapidapi-key": key
        }
    };
    return opts;
}

/**
 * Class for requesting data from API.
 */
class RapidAPI {
    constructor(apiKey) {
        this.apiKey = apiKey;
    }

    /**
     * @returns { Promise<ByCountryData> }
     */
    getCasesByCountry() {
        return getData(makeOpts(this.apiKey, CASES_BY_COUNTRY));
    }

    /**
     * @returns { Promise<WorldTotalData> }
     */
    getWorldTotal() {
        return getData(makeOpts(this.apiKey, WORLD_TOTAL));
    }
}

module.exports = RapidAPI;